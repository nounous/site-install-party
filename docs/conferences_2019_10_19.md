# Install Party du 19 octobre 2019

Ces oeuvres (enregistrements audio et vidéo) sont mises à disposition selon les termes de la Licence Creative Commons Attribution - Pas de Modification 4.0 International. 

[![Creative Commons License](https://i.creativecommons.org/l/by-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nd/4.0/)

| Intervenant                                            | Thème de la conférence         | Enregistrement | Horaire |
|--------------------------------------------------------|--------------------------------|----------------|---------|
| [Juliusz Chroboczek](http://www.pps.univ-paris-diderot.fr/~jch/) | Normes et logiciels libres | <https://ftps.crans.org/events/Install-party/2019/2019_10_19_Juliusz_Chroboczek.webm>  | 14:00   |
| [Jordan Delorme](https://delorme.pro/) | Politique de sécurité des systèmes d'informations : de la politique vers une pratique | <https://ftps.crans.org/events/Install-party/2019/2019_10_19_Jordan_Delorme.webm>  | 15:20   |
| Veronique Bonnet | Documents, échantillons, logiciels libres | <https://ftps.crans.org/events/Install-party/2019/2019_10_19_Bonnet.webm>  | 16:00   |

<video style="max-width:100%;height:auto" preload="metadata" controls>
<source src="https://ftps.crans.org/events/Install-party/2019/2019_10_19_Juliusz_Chroboczek.webm" type="video/webm">
</video>

<video style="max-width:100%;height:auto" preload="metadata" controls>
<source src="https://ftps.crans.org/events/Install-party/2019/2019_10_19_Jordan_Delorme.webm" type="video/webm">
</video>

<video style="max-width:100%;height:auto" preload="metadata" controls>
<source src="https://ftps.crans.org/events/Install-party/2019/2019_10_19_Bonnet.webm" type="video/webm">
</video>

