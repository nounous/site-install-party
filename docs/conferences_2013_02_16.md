# Install Party du 16 février 2013

Ces oeuvres (enregistrements audio et vidéo) sont mises à disposition selon les termes de la Licence Creative Commons Attribution - Pas de Modification 4.0 International. 

[![Creative Commons License](https://i.creativecommons.org/l/by-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nd/4.0/)

| Intervenant                                                                      | Thème de la conférence                                                 | Enregistrement                                                                                                                                                       | Horaire |
|----------------------------------------------------------------------------------|------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------|
| Morgane Jançon, présidente de [FedeRez](https://www.federez.net/)                | Présentation de la fédération de réseaux étudiants             FedeRez | <https://ftps.crans.org/events/Install-party/2013/2013_02_16_Morgane_Jancon.webm>             | 14:00   |
| Jean-Benoist Leger                                                               | Introduction à GnuPG                                                   | <https://ftps.crans.org/events/Install-party/2013/2013_02_16_JeanBenoist_Leger.webm> |   14:30   |
| [Juliusz Chroboczek](http://www.pps.univ-paris-diderot.fr/~jch/)                 | IPv6, le nouveau protocole de l'Internet                               | <https://ftps.crans.org/events/Install-party/2013/2013_02_16_Juliuz_Chroboczek.webm> | 15:30    |
| [Benjamin Bayart](http://edgard.fdn.fr/), président de [FDN](http://www.fdn.fr/) | Neutralité des réseau, impacte d'internet sur la société               | <https://ftps.crans.org/events/Install-party/2013/2013_02_16_Benjamin_Bayart.webm>, [slides](https://ftps.crans.org/events/Install-party/2013/2013_02_16_Benjamin_Bayart-slides.webm) | 17:00   |

Depuis le début des années 1980, le protocole fondamental de l'Internet est le Protocole Internet version 4 (IPv4). IPv4 est limité à quatre milliards d'adresses, ce qui n'est plus suffisant aujourd'hui. De ce fait, l'Internet est aujourd'hui en pleine transition vers un nouveau protocole, le Protocole Internet version 6 (IPv6).
Dans cet exposé, je dirai quelques mots sur l'architecture de l'Internet, et je décrirai notamment le principe de la commutation de paquets. J'expliquerai ensuite pourquoi IPv4 arrive en fin de vie, et quelles sont les conséquences pratiques de ses limitations aujourd'hui, notamment vis-à-vis des serveurs personnels et des applications pair-à-pair. Enfin, je décrirai le protocole IPv6, et ferai quelques hypothèses sur les conséquences pratiques que la transition vers IPv6 pourra avoir pour les utilisateurs. 

<video style="max-width:100%;height:auto" preload="metadata" controls>
<source src="https://ftps.crans.org/events/Install-party/2013/2013_02_16_Morgane_Jancon.webm" type="video/webm">
</video>

<video style="max-width:100%;height:auto" preload="metadata" controls>
<source src="https://ftps.crans.org/events/Install-party/2013/2013_02_16_JeanBenoist_Leger.webm" type="video/webm">
</video>

<video style="max-width:100%;height:auto" preload="metadata" controls>
<source src="https://ftps.crans.org/events/Install-party/2013/2013_02_16_Juliuz_Chroboczek.webm" type="video/webm">
</video>

<video style="max-width:100%;height:auto" preload="metadata" controls>
<source src="https://ftps.crans.org/events/Install-party/2013/2013_02_16_Benjamin_Bayart.webm" type="video/webm">
</video>

