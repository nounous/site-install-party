# Site de l'Install-Party

Toute la documentation liée à l'install party Cr@ns.

## Compiler la documentation chez soi

Il faut installer `mkdocs` et `mkdocs-material`.
Ensuite dans le dossier vous pouvez simplement appeler `mkdocs serve`.
