# Install Party du 14 janvier 2012

Ces oeuvres (enregistrements audio et vidéo) sont mises à disposition selon les termes de la Licence Creative Commons Attribution - Pas de Modification 4.0 International. 

[![Creative Commons License](https://i.creativecommons.org/l/by-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nd/4.0/)

| Intervenant                                                   | Thème de la conférence                                     | Horaire |
|---------------------------------------------------------------|------------------------------------------------------------|---------|
| [Luca Saiu](http://www-lipn.univ-paris13.fr/~saiu/)           | Le mouvement du logiciel libre                             | 14:00   |
| [Stéphane Bortzmeyer](http://www.bortzmeyer.org/)             | Peut-on se passer de moteurs de recherche ?                | 15:00   |
| [Jean-Vincent Loddo](http://www-lipn.univ-paris13.fr/~loddo/) | Tirer les ficelles de l'architecture TCP/IP avec Marionnet | 16:00   |

Le logiciel libre est basé sur quatre libertés : la liberté d'utiliser un programme pour n'importe quelle finalité, la liberté de le modifier, celle d'en redistribuer des copies exactes, et celle d'en distribuer des copies modifiées qui contribueront à son développement. Né en 1983 de l'idéalisme pragmatique d'une seule personne essayant de reconstruire une communauté centrée sur les droits des utilisateurs, le logiciel libre profite aujourd'hui d'un développement exceptionnel en quantité et qualité des productions. Nous allons parler de l'histoire et de la philosophie de ce mouvement fascinant, des aspects légaux et pratiques de l'utilisation du logiciel libre et de son développement. 

Aujourd'hui, beaucoup d'utilisateurs dépendent entièrement d'un moteur de recherche pour toute navigation sur l'Internet. On entend même des enseignants de collège dire aux élèves " Pour aller sur Wikipedia, tapez "wikipedia" dans Google " Pourquoi est-ce une mauvaise idée ? Quels sont les inconvénients des moteurs de recherche ? Que se passe-t-il lorsqu'une panne ou la censure modifie le résultat d'une recherche ? Quels sont les points forts des moteurs de recherche, où ils sont utiles ? à quoi sert le DNS et pourquoi est-ce important de comprendre les noms de domaine ? 
[Une amusante vidéo qui illustre bien les inconvénients de la dépendance envers Google.](http://normanfaitdesvideos.com/2011/10/19/maintenant-jai-google/)

Marionnet est un logiciel permettant de définir, configurer, exécuter et contrôler un réseau virtuel constitué d'ordinateurs utilisant le système d'exploitation GNU/Linux, de concentrateurs, de commutateurs, et de routeurs. Avec Marionnet, il est possible d'expérimenter la mise en oeuvre complète d'un réseau local : le projet, le ciblage, le lancement, la configuration, l'administration, l'étude de protocoles et le test de services ou d'applications. Il permet de pratiquer, analyser et contrôler les différentes couches réseaux qui constituent l'architecture DoD (TCP/IP) : depuis le niveau physique jusqu'au niveau application. Adopté dans une structure universitaire, Marionnet permet de réduire l'utilisation des vraies salles de " TP réseau ", constituées d'équipements souvent onéreux et difficiles à maintenir en parfait état de marche. Il permet aussi aux étudiants de travailler à distance ou dans les salles informatiques ordinaires en accès libre.

