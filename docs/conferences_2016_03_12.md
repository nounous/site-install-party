# Install Party du 12 mars 2016

Ces oeuvres (enregistrements audio et vidéo) sont mises à disposition selon les termes de la Licence Creative Commons Attribution - Pas de Modification 4.0 International. 

[![Creative Commons License](https://i.creativecommons.org/l/by-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nd/4.0/)

| Intervenant                                            | Thème de la conférence         | Enregistrement | Horaire |
|--------------------------------------------------------|--------------------------------|----------------|---------|
| Alain Imbaud, Président de l'association Musique Libre | Musique libre et culture libre | <https://ftps.crans.org/events/Install-party/2016/2016_03_12_Alain_Imbaud.mp4>  | 14:30   |

<video style="max-width:100%;height:auto" preload="metadata" controls>
<source src="https://ftps.crans.org/events/Install-party/2016/2016_03_12_Alain_Imbaud.mp4" type="video/webm">
</video>

