# Les affiches

[![Install Party Octobre 2021](affiches/ip_2021-10.jpg)](affiches/ip_2021-10.pdf)

## Les affiches des précédentes Install-Parties

[![Install Party Octobre 2019](affiches/ip_2019-10.jpg)](affiches/ip_2019-10.pdf)
[![Install Party Mars 2017](affiches/ip_2017-03.jpg)](affiches/ip_2017-03.pdf)
[![Install Party Mars 2016](affiches/ip_2016-03.jpg)](affiches/ip_2016-03.pdf)
[![Install Party Mars 2015](affiches/ip_2015-03.jpg)](affiches/ip_2015-03_large.jpg)
[![Install Party Février 2013](affiches/ip_2013-02.jpg)](affiches/ip_2013-02.pdf)
[![Install Party Janvier 2012](affiches/ip_2012-01.jpg)](affiches/ip_2012-01.pdf)
[![Install Party Janvier 2011](affiches/ip_2011-01.jpg)](affiches/ip_2011-01.pdf)
[![Install Party Février 2010](affiches/ip_2010-02.jpg)](affiches/ip_2010-02_large.png)
[![Install Party Novembre 2008](affiches/ip_2008-11.jpg)](affiches/ip_2008-11_large.jpg)
[![Install Party Novembre 2007](affiches/ip_2007-11.jpg)](affiches/ip_2007-11_large.png)
[![Install Party Novembre 2006](affiches/ip_2006-11.jpg)](affiches/ip_2006-11_large.png)
[![Install Party Novembre 2004](affiches/ip_2004-11.jpg)](affiches/ip_2004-11_large.png)
[![Install Party Mars 2004](affiches/ip_2004-03.jpg)](affiches/ip_2004-03_large.png)
[![Install Party Novembre 2003](affiches/ip_2003-11.jpg)](affiches/ip_2003-11_large.png)
[![Install Party Novembre 2002](affiches/ip_2002-11.jpg)](affiches/ip_2002-11_large.png)
[![Install Party Octobre 2000](affiches/ip_2000-10.jpg)](affiches/ip_2000-10_large.png)

