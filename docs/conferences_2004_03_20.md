# Install Party du 20 mars 2004

Ces oeuvres (enregistrements audio et vidéo) sont mises à disposition selon les termes de la Licence Creative Commons Attribution - Pas de Modification 4.0 International.

[![Creative Commons License](https://i.creativecommons.org/l/by-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nd/4.0/)

| Intervenant          | Thème de la conférence                | Enregistrement | Horaire |
|----------------------|---------------------------------------|----------------|---------|
| ?                    | Présentation de l'environnement KDE   | <https://ftps.crans.org/events/Install-party/2004/2004_03_20_Presentation_KDE.webm> | ?       |
| Julien Blache        | Présentation du projet Debian         | <https://ftps.crans.org/events/Install-party/2004/2004_03_20_Julien_Blache_Presentation_Debian.webm> | ?       |
| Denis Barbier        | Localisation dans Debian              | <https://ftps.crans.org/events/Install-party/2004/2004_03_20_Denis_Barbier_Localisation_dans_Debian.webm> | 15h30   |
| ?                    | VideoLan                              | <https://ftps.crans.org/events/Install-party/2004/2004_03_20_VideoLan.webm> | 16h30   |
| Ludovic Pénet        | Contexte législatif du logiciel libre | <https://ftps.crans.org/events/Install-party/2004/2004_03_20_Ludovic_Penet_Contexte_legislatif_logiciel_libre.webm> | 17h     |

