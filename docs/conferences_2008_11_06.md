# Install Party du 6 novembre 2008

Ces oeuvres (enregistrements audio et vidéo) sont mises à disposition selon les termes de la Licence Creative Commons Attribution - Pas de Modification 4.0 International. 

[![Creative Commons License](https://i.creativecommons.org/l/by-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nd/4.0/)

| Intervenant     | Thème de la conférence                                 | Horaire |
|-----------------|--------------------------------------------------------|---------|
| Emmanuel Seyman | Qu'est-ce que le logiciel libre ?                      | 14:30   |
| Benjamin Jean   | Les enjeux économiques et juridiques du logiciel libre | 16:00   |

