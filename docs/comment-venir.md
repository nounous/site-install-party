# Comment venir à l'ENS Paris-Saclay ?

L'Install-Party a lieu à partir de 10h à l'Atrium Germaine Tillion de l'ENS Paris-Saclay,
4 Avenue des Sciences, Gif-Sur-Yvette, Île-de-France.

<iframe width="720" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=2.1588134765625004%2C48.71111972247925%2C2.170786857604981%2C48.715282287625314&amp;layer=mapnik&amp;marker=48.71320104810086%2C2.1648001670837402" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=48.71320&amp;mlon=2.16480#map=17/48.71320/2.16480">View Larger Map</a></small>

<!--
## En voiture

Depuis l'Autoroute A6, suivre l'A6b, sortir à Fresnes.

Suivre la RN 86 jusqu'à Bagneux et rejoindre la RN 20.

Sur la RN 20 (avenue Aristide-Briand), suivre la direction Cachan, puis le fléchage ENS (à droite, direction Paris).

## En RER

Depuis Paris, prendre le RER B, direction Massy-Palaiseau ou Robinson, jusqu'à la station Bagneux.

Suivre ensuite l'avenue Pont-Royal, puis l'avenue de Chateaubriand jusqu'à l'entrée principale de l'école.

## En Bus

Ligne 184 Porte d'Italie / L'Hay-les-Roses : arrêt Division Leclerc - Camille Desmoulins.

Ligne 187 Porte d'Orléans / Fresnes / Antony : arrêt Division Leclerc - Camille Desmoulins.

Ligne 197 Porte d'Orléans / Bourg-la-Reine : arrêt RER Bagneux (puis 5 minutes à pied).

Ligne 162 Villejuif / Cachan / Meudon : arrêt Mairie de Cachan (puis 10 minutes à pied).
-->

