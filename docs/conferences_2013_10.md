# 15 ans du Cr@ns

Pour fêter ses 15 ans, le CRANS organise un événement autour d'Internet et du
logiciel libre, les 19 et 20 octobre 2013, à l'ENS Cachan (hall Villon).
Cycle de conférences générales ou techniques, ateliers d'initiations à divers
outils du libre (apportez votre machine !) se dérouleront tout au long du
week-end. Un interlude d'activités ludiques occupera la soirée du samedi.

## Samedi 19 octobre

  * 9h-11h : ouverture de l'événement
  * 11h-11h30 : présentation et historique du Crans
  * 12h-13h30 : buffet
  * 13h-14h30 : atelier GnuPG
  * 14h-15h : conférence de [Bastien Guerry](http://bzg.fr/) sur le thème du logiciel libre dans l'éducation

    La conférence de Bastien Guerry portera sur les ressources pédagogiques libres : de Sésamath à Wikipédia, ces ressources ont déjà une histoire. Mais ont-elles encore un avenir ? Si oui, lequel, et comment y participer ? Walter Bender, fondateur de la plate-forme pédagogique libre Sugar (http://sugarlabs.org) dit : « L'éducation est aussi essentielle à la liberté que le libre est essentiel à l'éducation. » Doit-on comprendre cela au pied de la lettre ? Si oui, quelles conséquences ? Un tour d'horizon d'une heure pour comprendre d'où viennent et où vont les ressources pédagogiques libres.

  * 15h15-16h45 : conférence "Internet et libertés après Snowden" par [Benjamin Sonntag](http://benjamin.sonntag.fr/fr), porte-parole et co-fondateur de [La Quadrature du Net](http://www.laquadrature.net/)
  * 17h-18h30 : conférence sur les FAI associatifs par [Benjamin Bayart](http://edgard.fdn.fr/), ancien président, aujourd'hui porte-parole de [FDN](http://www.fdn.fr/)
  * 18h-00h30 : activités ludiques

## Dimanche 20 octobre

  * 9h30 : début du petit-déjeuner
  * 10h30-12h : conférence "DNSSec et sécurité des sites web" par
    [Stéphane Bortzmeyer](http://www.bortzmeyer.org/),
    ingénieur informaticien à l'[AFNIC](http://www.afnic.fr/)
    et contributeur de Wikipédia.

    Lorsqu'on parle de sécurité du Web, l'accent est souvent mis sur les attaques spectaculaires permettant de prendre le contrôle du site (par exemple par injection SQL) et de modifier son contenu. Ces attaques sont en effet très fréquentes mais ne représentent pas la totalité des menaces. L'exposé mettra l'accent sur trois types de menaces parfois sous-estimées : les attaques par déni de service, celles passant par les noms de domaines (détournement, ou empoisonement) et celles utilisant les certificats. On parlera aussi des solutions, comme DNSSEC (contre l'empoisonnement DNS).

  * 12h : buffet
  * à partir de 12h : ateliers Blender et Beamer
  * 13h45-14h15 : conférence sur OpenWrt, par des membres du Crans
  * 14h30-16h : conférence sur Debian par
    [Stefano Zacchiroli](http://upsilon.cc/~zack/),
    maître de conférence à l'Université Paris Diderot (Paris 7),
    développeur et ancien chef de projet [Debian](http://www.debian.org/)
  * 16h30-18h : signing-party [Instructions](http://tanguy.ortolo.eu/blog/article111/install-party-15ans-crans)
  * 15h30-18h : ateliers (voir détail ci-dessous)

## Ateliers

  * Atelier Blender, animé par Yohan Le Diraison, Maître de Conférences à l'Université de Cergy-Pontoise
  * Initiation à GPG ; signing party
  * Introduction au logiciel de retouche photo et création graphique Gimp
  * Prise en main de Beamer

## Activités ludiques

Afin de donner une touche conviviale aux festivités, nous organiserons samedi soir plusieurs activités type jeu de société et une LAN. 

## Enregistrements

Ces oeuvres (enregistrements audio et vidéo) sont mises à disposition selon les termes de la Licence Creative Commons Attribution - Pas de Modification 4.0 International. 

[![Creative Commons License](https://i.creativecommons.org/l/by-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nd/4.0/)

<audio style="max-width:100%;height:auto" preload="metadata" controls>
<source src="https://ftps.crans.org/pub/events/2013%2015ans/DNSSec_et_securite_des_sites_web_%28Stephane_Bortzmeyer%29.oga" type="video/webm">
</audio>

<audio style="max-width:100%;height:auto" preload="metadata" controls>
<source src="https://ftps.crans.org/pub/events/2013%2015ans/Debian%2C_fonctionnement_technique_et_social_%28Stefano_Zacchiroli%29.oga" type="video/webm">
</audio>

<video style="max-width:100%;height:auto" preload="metadata" controls>
<source src="https://ftps.crans.org/pub/events/2013%2015ans/Introduction_a_GPG_%28membres_du_crans%29.webm" type="video/webm">
</video>

<audio style="max-width:100%;height:auto" preload="metadata" controls>
<source src="https://ftps.crans.org/pub/events/2013%2015ans/OpenWrt_et_ses_usages_aux_CRANS_%28Daniel_Stan%29.oga" type="video/webm">
</audio>

<video style="max-width:100%;height:auto" preload="metadata" controls>
<source src="https://ftps.crans.org/pub/events/2013%2015ans/fournisseur_d%27acces_internet_associatif_%28Benjamin_Bayart%29.webm" type="video/webm">
</video>

<video style="max-width:100%;height:auto" preload="metadata" controls>
<source src="https://ftps.crans.org/pub/events/2013%2015ans/internet_et_liberte%2C_l%27apres_Edward_Snowden_%28Benjamin_Sonntag%29.webm" type="video/webm">
</video>

<video style="max-width:100%;height:auto" preload="metadata" controls>
<source src="https://ftps.crans.org/pub/events/2013%2015ans/libre_et_education_%28Bastien_Guerry%29.webm" type="video/webm">
</video>

