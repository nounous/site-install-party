# Install Party du 13 février 2010

Ces oeuvres (enregistrements audio et vidéo) sont mises à disposition selon les termes de la Licence Creative Commons Attribution - Pas de Modification 4.0 International. 

[![Creative Commons License](https://i.creativecommons.org/l/by-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nd/4.0/)

| Intervenant     | Thème de la conférence                 | Horaire |
|-----------------|----------------------------------------|---------|
| Pierre Chambart | L'esprit du Libre                      | 14:00   |
| Gaël Varoquaux  | Python dans le domaine scientifique    | 15:00   |
| Michaël Scherer | Jabber et les messageries instantanées | 16:00   |

