# Le Crans vous invite à son Install-Party

> Samedi 10 décembre 2022, les étudiants du Crans (association réseau étudiante) vous accueilleront à l'ENS Paris-Saclay (anciennement ENS Cachan).

![](img/crans.svg){: style="height:150px;margin-right:4em"}
![](img/tux.svg){: style="height:150px"}

Chaque année, le [Crans](https://www.crans.org/) organise l'install-party de l'[ENS Paris-Saclay](https://ens-paris-saclay.fr/). C'est l'occasion de passer à GNU/LINUX en douceur ! Cette année, notre install-party aura lieu le samedi 10 décembre 2022 à l'Atrium Germaine Tillion[^1] de l'ENS Paris-Saclay.
Le programme des conférences est disponible dans la section des conférences.

[^1]: Atrium Germaine Tillion, ENS Paris-Saclay, 4 Avenue des Sciences, Gif-Sur-Yvette, Île-de-France

Nous vous proposons d'installer GNU/Linux sur les ordinateurs (ils cohabitent avec d'autres systèmes d'exploitation sans difficultés). De plus durant toute la journée, les curieux et les indécis sont invités à venir discuter avec la communauté des logiciels libres.
Les « linuxiens » présents se feront un plaisir de répondre aux questions et aider les débutants dans leurs premiers pas.

*Vous trouverez sur ce site des informations relatives à la prochaine install-party ainsi que des archives des anciennes.*

Pour toute information supplémentaire, n'hésitez pas à nous contacter en écrivant à: <install-party@crans.org>.

![](img/logo.png)
