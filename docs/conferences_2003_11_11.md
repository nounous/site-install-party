# Install Party du 11 novembre 2003

Ces oeuvres (enregistrements audio et vidéo) sont mises à disposition selon les termes de la Licence Creative Commons Attribution - Pas de Modification 4.0 International.

[![Creative Commons License](https://i.creativecommons.org/l/by-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nd/4.0/)

| Intervenant          | Thème de la conférence              | Enregistrement |
|----------------------|-------------------------------------|----------------|
| Thomas Basset        | VideoLan                            | <https://ftps.crans.org/events/Install-party/2003/2003_11_11_Thomas_Basset_VideoLan.webm> |
| Manuel Sabban        | Structure réseau du Crans           | <https://ftps.crans.org/events/Install-party/2003/2003_11_11_Manuel_Sabban_Le_Crans.webm> |
| Ralf Treinen         | Que fait un développeur Debian      | <https://ftps.crans.org/events/Install-party/2003/2003_11_11_Ralf_Treinen_Que_fait_un_developpeur_Debian.webm> |
| Stephane Dubreil     | les environnements graphiques       | <https://ftps.crans.org/events/Install-party/2003/2003_11_11_Stephane_Dubreil_Les_environnements_graphiques.webm> |
| Cedric Blancher      | Les capacités réseau du noyau Linux | <https://ftps.crans.org/events/Install-party/2003/2003_11_11_Cedric_Blancher_Les_capacites_reseau_du_noyau_Linux.webm> |
| Lionel Dricot        | Utilisation d'OpenOffice.org        | <https://ftps.crans.org/events/Install-party/2003/2003_11_11_Lionel_Dricot_OpenOffice.webm> |
| Frederic Pauget      | Comment monter un réseau chez moi   | <https://ftps.crans.org/events/Install-party/2003/2003_11_11_Frederic_Pauget_reseau_chez_soi.webm> |
| Joris Van der Hoeven | GNU TexMacs                         | <https://ftps.crans.org/events/Install-party/2003/2003_11_11_Joris_Van_der_Hoeven_gnu_texmacs.webm> |
| Emmanuel Quemener    | Le libre à l'ENS Cachan             | <https://ftps.crans.org/events/Install-party/2003/2003_11_11_Emmanuel_Quemener_Le_libre_a_ENS_Cachan.webm> |

