# Voici quelques conseils pour bien réussir son install party

**Faites une copie de sauvegarde** de vos documents importants, on ne sait jamais ce qu'il peut arriver.
Apportez **un ordinateur complet** : unité centrale, mais aussi souris, clavier et écran. Pour un ordinateur portable, pensez à apporter son **chargeur**.
Apportez toutes **les documentations techniques** sur votre matériel : caractéristiques de votre écran, de votre carte graphique...

Venez le plus tôt possible, nous aurons alors tout le temps pour faire tranquillement votre installation.
Lisez, imprimez et remplissez [cette décharge](doc/decharge.pdf), nous vous demanderons de la signer avant de procéder à l'installation.

## Liens utiles

  * Les forums Ubuntu : <https://forum.ubuntu-fr.org/>,
  * Le Wiki de Debian : <https://wiki.debian.org/>,
  * Le Wiki d'ArchLinux : <https://wiki.archlinux.org/>.

