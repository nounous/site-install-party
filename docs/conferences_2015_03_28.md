# Install Party du 28 mars 2015

Ces oeuvres (enregistrements audio et vidéo) sont mises à disposition selon les termes de la Licence Creative Commons Attribution - Pas de Modification 4.0 International. 

[![Creative Commons License](https://i.creativecommons.org/l/by-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nd/4.0/)

| Intervenant                                        | Thème de la conférence          | Enregistrement                                                             | Horaire |
|----------------------------------------------------|---------------------------------|----------------------------------------------------------------------------|---------|
| Pablo Rauzy, doctorant en informatique             | Open Access                     | <https://ftps.crans.org/events/Install-party/2015/2015_03_28_Pablo_Rauzy.mp4>                                  | 14:30   |
| Ralf Treinen, enseignant-chercheur à Paris Diderot | Qualité des distributions linux | <https://ftps.crans.org/events/Install-party/2015/2015_03_28_Ralf_Treinen.mp4> | 15:40   |
| Veronique Bonnet                                   | Missions et succes de l'april   | <https://ftps.crans.org/events/Install-party/2015/2015_03_28_Bonnet.mp4> | 17:00   |

<video style="max-width:100%;height:auto" preload="metadata" controls>
<source src="https://ftps.crans.org/events/Install-party/2015/2015_03_28_Pablo_Rauzy.mp4" type="video/webm">
</video>

<video style="max-width:100%;height:auto" preload="metadata" controls>
<source src="https://ftps.crans.org/events/Install-party/2015/2015_03_28_Ralf_Treinen.mp4" type="video/webm">
</video>

<video style="max-width:100%;height:auto" preload="metadata" controls>
<source src="https://ftps.crans.org/events/Install-party/2015/2015_03_28_Bonnet.mp4" type="video/webm">
</video>

