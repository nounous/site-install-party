# Install Party du 17 novembre 2007

Ces oeuvres (enregistrements audio et vidéo) sont mises à disposition selon les termes de la Licence Creative Commons Attribution - Pas de Modification 4.0 International. 

[![Creative Commons License](https://i.creativecommons.org/l/by-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nd/4.0/)

| Intervenant        | Thème de la conférence                                                                     | Enregistrement                                                       |
|--------------------|--------------------------------------------------------------------------------------------|----------------------------------------------------------------------|
| Olivier Cleynen    | Code source, anarchisme et piratage : comment comprendre le logiciel libre tout de travers | |
| Juliusz Chroboczek | Le developpement de logiciel libre | <https://ftps.crans.org/events/Install-party/2007/Juliusz_CHROBOCZEK.ogg> |

