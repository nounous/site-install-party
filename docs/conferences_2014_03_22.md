# Install Party du 22 mars 2014

Ces oeuvres (enregistrements audio et vidéo) sont mises à disposition selon les termes de la Licence Creative Commons Attribution - Pas de Modification 4.0 International. 

[![Creative Commons License](https://i.creativecommons.org/l/by-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nd/4.0/)

| Intervenant                         | Thème de la conférence                  | Enregistrement                                                        | Horaire |
|-------------------------------------|-----------------------------------------|-----------------------------------------------------------------------|---------|
| Laurent Séguin, président de l'[AFUL](https://aful.org/) | Le logiciel libre, vecteur de confiance | <https://ftps.crans.org/events/Install-party/2014/2014_03_22_Seguin.mp3> | 14:00   |

Nous le disons depuis des années, un logiciel non libre fait du mal à ses utilisateurs. Hormis les aspects informatiques purs, tels que la prise en otage du patrimoine numérique que les utilisateurs confient à ces logiciels, nous n'avions que des soupçons, que de faibles preuves, sur un mal encore plus fort et notre discours était naturellement orienté sur « choisissez du libre, car le logiciel libre ne vous fera pas sciemment du mal ». Les révélations d'Edward Snowden ont donné plus que corps à notre discours. En apportant des preuves de l'espionnage massif des populations par des États, via leurs services de renseignement, notamment par l'inclusion de code malicieux dans les logiciels et matériels, nos craintes et doutes ont été incommensurablement dépassée par l'ampleur de la réalité. Même le logiciel libre n'est pas forcément épargné, et chaque faille trouvée depuis ces révélations instaure un doute sur « erreur de programmation ? » ou « faille volontairement insérée ? ». Cette conférence aura pour objet de revenir sur les fondamentaux du logiciel libre, d'éclairer sur comment aborder l'informatique à l'ère post-Snowden, comment et pourquoi le logiciel libre est un vecteur indispensable pour restaurer la confiance et le rôle que doivent jouer les utilisateurs. 

