# Install Party du 15 janvier 2011

Ces oeuvres (enregistrements audio et vidéo) sont mises à disposition selon les termes de la Licence Creative Commons Attribution - Pas de Modification 4.0 International. 

[![Creative Commons License](https://i.creativecommons.org/l/by-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nd/4.0/)

| Intervenant        | Thème de la conférence     | Enregistrements                                                                                                                                                    | Horaire |
|--------------------|----------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------|
| Michel Blockelet   | Le monde du Logiciel Libre | <https://ftps.crans.org/events/Install-party/2011/15-01-2011_LogicielLibre_MBlockelet.ogg>  | 14:30   |
| Nicolas Dandrimont | Le projet OpenStreetMap    | <https://ftps.crans.org/events/Install-party/2011/15-01-2011_OpenStreetMap_NDandrimont.ogg> | 15:30   |
| Michaël Scherer    | L'autohébergement          | <https://ftps.crans.org/events/Install-party/2011/15-01-2011_AutoHebergement_MScherer.ogg>  | 17:00   |

Les vidéos sont disponibles ici : <https://ftps.crans.org/events/Install-party/2011/video/>.

